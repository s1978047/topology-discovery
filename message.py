# A message in the network
# has a message id (integer), origin node ID (integer), destination node ID (integer), hopcount (integer),
# timestamp (long), path (list of integers)

class Message:

	def __init__(self, id, origin, destination, hopcount, timestamp, path):
		self.id = id
		self.origin = origin
		self.destination = destination
		self.hopcount = hopcount
		self.timestamp = timestamp
		self.path = path

	def incr_hop_count(self):
		self.hopcount += 1

	def print(self):
		print("Message " + self.id + " from node " + self.origin + " to node " + self.destination + " at " +
			self.timestamp + " with hopcount " + str(self.hopcount) + " path " + str(self.path))
