# A node in a network
# has a unique ID (integer) a list of neighbors IDs (integer array), and a list of messages (Message arraylist)
# Each node also has its own Agent
class Node:

	def __init__(self, id):
		self.id = id
		self.neighbors = []
		self.messages = []
		self.msgCnt = 0
		# [(1, [2,3]), (2, [3,4]),....]
		self.routingtable = [(self.id, [])]
		# the amount of messages send for the flooding algorithm
		self.floodingCnt = 0
		# counter of metadata, used for mapping the network, sent by this node
		self.metadata = 0
		self.finalMsgCnt = 0
		# list containing all message IDs that gave this node new information about the network
		self.msgList = []
		# list containing the msg received # for each message that gave this node new information about the network
		self.msgListnr = []
		self.timestampschanged = []
		self.routingtablesize = 0
		self.metadatausefull = 0
		self.overheadCnt = 0
		self.q1 = None
		self.q2 = None
		self.q3 = None

	# add a neighbor to this node, and update routing table
	def add_neighbor(self, neighbor):
		self.routingtablesize += 1
		self.neighbors.append(neighbor)
		# update routing table
		self.routingtable[0][1].append(neighbor.id)

	# start the flooding algorithm
	def flood(self):
		for neighbor in self.neighbors:
			# send each neighbor, your routing table
			# send routing table to neighbor
			self.floodingCnt += 1
			neighbor.receive_routing_table(self.routingtable, self.id)
		print("flooding done " + str(len(self.routingtable)) + "messages sent " + str(self.floodingCnt))

	# update the routing table, return whether it changed
	def update_routing(self, routing_new):
		changed = False
		for entry in routing_new:
			# if we found a new entry, add it the routing table
			if entry not in self.routingtable:
				changed = True
				self.routingtable.append(entry)
		return changed

	# receive routing table from neighbor neighbor_id
	def receive_routing_table(self, routing_new, neighbor_id):
		# update routing table, and if it remained unchanged do nothing
		if not self.update_routing(routing_new):
			# if we have a complete map of the network
			if len(self.routingtable) == 100:
				print("Node: " + str(self.id) + " has a complete map!\nAmount of neighbor lists sent: " + str(
					self.floodingCnt))
		else:  # if the routing table changed
			# flood routing table
			self.broadcast(neighbor_id)

	# broadcast routing table to neighbors, except the neighbor it received the update from
	def broadcast(self, neighbor_rcvd):
		# send to all neighbors
		for neighbor in self.neighbors:
			# send each neighbor, your routing table
			if neighbor != neighbor_rcvd:
				# send routing table to neighbor
				self.floodingCnt += 1
				# TODO should happen concurrently
				neighbor.receive_routing_table(self.routingtable, self.id)
			# TODO get required metadata of list and add it to counter

	# get the 25, 50, and 75 % of the total network size
	def quarters(self, q1, q2, q3):
		self.q1 = q1
		self.q2 = q2
		self.q3 = q3

	# build routing table based on received message
	def receivemsg(self, message):
		self.msgCnt += 1
		changed = False
		self.overheadCnt += len(message.path)

		# loop over all nodes in the path and update routing tables
		# final message in the path can be skipped, since that is the node himself
		for i in range(len(message.path) - 1):  # loop over the path of the message
			# get index n from [(x, [...])] where x == message.path[i]
			index = next((n for n, t in enumerate(self.routingtable) if t[0] == message.path[i]), None)
			# entry = next[n for n, t in enumerate(self.routingtable) if t[0] == message.path[i]].pop()
			if index is not None:  # if we found the node in the list
				entry = self.routingtable[index]
				# print("found node: " + str(self.routingtable[entry]))

				if not (message.path[i + 1]) in entry[1]:  # if we do not have node i + 1 in the neighbor list, add it
					self.routingtablesize += 1
					entry[1].append(message.path[i + 1])  # node i + 1 gets added to the neighbors of node i
					changed = True
				if i > 0 and (
						not (message.path[i - 1]) in entry[
							1]):  # if we do not have node i - 1 in the neighbor list, add it
					self.routingtablesize += 1
					entry[1].append(message.path[i - 1])  # node i - 1 gets added to the neighbors of node i
					changed = True
			else:  # if node message.path[i] is not in the entries
				# self.routingtable.append((message.path[i], []))
				changed = True
				if i > 0:
					self.routingtablesize += 2
					self.routingtable.append((message.path[i], [message.path[i - 1], message.path[i + 1]]))
				else:
					self.routingtablesize += 1
					self.routingtable.append((message.path[i], [message.path[i + 1]]))

		if changed:  # if we changed our routing table
			self.finalMsgCnt = self.msgCnt
			self.msgList.append(message.id)
			self.msgListnr.append(self.msgCnt)
			self.timestampschanged.append(message.timestamp)
			self.metadatausefull += len(message.path)

			if self.routingtablesize > self.q1:
				self.q1t = message.timestamp
				self.q1 = float('inf')
				self.q1m = self.msgCnt
			elif self.routingtablesize > self.q2:
				self.q2t = message.timestamp
				self.q2 = float('inf')
				self.q2m = self.msgCnt
			elif self.routingtablesize > self.q3:
				self.q3t = message.timestamp
				self.q3 = float('inf')
				self.q3m = self.msgCnt

	def print(self):
		self.msgList.sort()
		self.routingtable.sort()
		print("\n-----------\nNode " + str(self.id))
		print("Neighbors:")
		for neighbor in self.neighbors:
			print("\tNode " + str(neighbor.id))
		print("Final message# received that changed the routing table: " + str(self.finalMsgCnt))
		print("Total messages received: " + str(self.msgCnt))
		print("Routing table: " + str(self.routingtable))
		print("ID of messages that gave this node new information about the network: " + str(self.msgList))
		print("Received # of messages that gave this node new information about the network" + str(self.msgListnr))
		print("\nSize network map: " + str(self.routingtablesize))
		print("Timestamps of getting 25%, 50%, 75% of the full network respectively: " + str(self.q1t) + " - " + str(
			self.q2t) + " - " + str(self.q3t))
