from node import Node
from message import Message
import networkx as nx
from matplotlib import pyplot as plt

# the limit of the amount of nodes a path may contain
limit = 999
# if true we limit our path by only keeping the first encountered nodes
first = True


def initialize():
	print("Initializing network...")
	nodes = []
	received = []
	G = nx.Graph()

	# create amount of required Node objects
	for i in range(0, 100):
		node = Node(i)
		nodes.append(node)
		G.add_node(i)
		# list of received messages for each node
		received.append([])
	# read event log report, and store all the events
	log = open("REMI.WSN_EventLogReport.txt", "r")
	events = log.read().splitlines()
	print("filtering and handling events...")

	print("Parsing neighbor connection events")
	# handle all neighbor connections events
	while "up" in events[0]:
		chunks = events[0].split(' ')
		n1 = int(chunks[2].split("_", 1)[1])
		n2 = int(chunks[3].split("_", 1)[1])
		neighbor1 = nodes[n1]
		neighbor2 = nodes[n2]
		# if this is a new neighbor
		neighbor1.add_neighbor(neighbor2)
		neighbor2.add_neighbor(neighbor1)
		G.add_edge(n1, n2)
		events.pop(0)
	print("Done parsing neighbor connection events")

	# get the size of the network (sum of the amount of neighbors for all nodes)
	count = 0
	for node in nodes:
		count += len(node.neighbors)
	# get the 25%, 50%, and 75% sizes of the network
	q1 = round(count / 4)
	q2 = round(count / 2)
	q3 = q1 + q2
	for node in nodes:
		node.quarters(q1, q2, q3)

	print("Parsing received messages")
	# filter events
	for event in events:
		last = event[len(event) - 1]  # last character in the event
		if last == 'R' or last == 'D':  # successfully received message event
			chunks = event.split(' ')
			id = int(chunks[4].split('A', 1)[1])
			origin = int(chunks[2].split("_", 1)[1])
			dest = int(chunks[3].split("_", 1)[1])
			timestamp = float(chunks[0])
			hopcount = 0
			# find msg rcvd event in origin
			for msg in received[origin]:
				if msg.id == id:
					hopcount = msg.hopcount + 1
					path = msg.path.copy()
					if len(path) < limit:  # if our path still has room
						path.append(dest)  # append path with destination of current message
					elif first:  # if first then only keep the first n hops
						pass  # path already has the first n hops in there
					else:  # if we only keep the last n hops
						path.pop(0)  # remove first hop in path
						path.append(dest)  # add current hop to the end of the path
					break
			else:  # if we found a new message
				path = [origin, dest]

			message = Message(id, origin, dest, hopcount, timestamp, path)
			received[dest].append(message)

			# send message to the destination send
			nodes[dest].receivemsg(message)

	print("Done parsing received messages")

	print("--- Printing nodes information ---")
	for node in nodes:
		node.print()

	print("Drawing graph...")
	nx.draw(G)
	plt.savefig("network.png")

	# get and print statistics
	print("Analyzing nodes...")
	routingtablesizes = []
	amountmessagesreceivedlist = []
	informationgainedlist = []

	# quarter averages for neighbor amount
	q1a = [[], [], [], [], [], [], [], [], []]
	q2a = [[], [], [], [], [], [], [], [], []]
	q3a = [[], [], [], [], [], [], [], [], []]

	# remove unconnected nodes
	nodes.remove(nodes[3])  # node 3
	nodes.remove(nodes[71])  # node 72
	overhead_total = 0
	overhead_useful = 0
	q3time = 0
	totalq1msgcnt = 0
	totalq2msgcnt = 0
	totalq3msgcnt = 0
	timestampsreceived = []
	for node in nodes:
		totalq1msgcnt += node.q1m
		totalq2msgcnt += node.q2m
		totalq3msgcnt += node.q3m
		overhead_total += node.overheadCnt
		overhead_useful += node.metadatausefull
		q1a[len(node.neighbors) - 1].append(node.q1t)
		q2a[len(node.neighbors) - 1].append(node.q2t)
		q3a[len(node.neighbors) - 1].append(node.q3t)
		timestampsreceived += node.timestampschanged
		if node.q3t is None:
			print(node.id)
		q3time += node.q3t
		routingtablesizes.append(node.routingtablesize)
		amountmessagesreceivedlist.append(node.msgCnt)
		informationgainedlist += node.msgListnr
	print("Average routingtable size: " + str(sum(routingtablesizes) / 98))
	print("Average amount of messages received: " + str(sum(amountmessagesreceivedlist) / 98))
	print("Average amount of messages that gave new information: " + str(len(informationgainedlist) / 98))

	print("1 neighbor nodes time required to fill (25%, 50%, 75%) of the network map: " + str(
		sum(q1a[0]) / len(q1a[0])) + " - " + str(sum(q2a[0]) / len(q2a[0])) + " - " + str(sum(q3a[0]) / len(q3a[0])))
	print("2 neighbor nodes time required to fill (25%, 50%, 75%) of the network map: " + str(
		sum(q1a[1]) / len(q1a[1])) + " - " + str(sum(q2a[1]) / len(q2a[1])) + " - " + str(sum(q3a[1]) / len(q3a[1])))
	print("3 neighbor nodes time required to fill (25%, 50%, 75%) of the network map: " + str(
		sum(q1a[2]) / len(q1a[2])) + " - " + str(sum(q2a[2]) / len(q2a[2])) + " - " + str(sum(q3a[2]) / len(q3a[2])))
	print("4 neighbor node time required to fill (25%, 50%, 75%) of the network map: " + str(
		sum(q1a[3]) / len(q1a[3])) + " - " + str(sum(q2a[3]) / len(q2a[3])) + " - " + str(sum(q3a[3]) / len(q3a[3])))
	print("5 neighbor node time required to fill (25%, 50%, 75%) of the network map: " + str(
		sum(q1a[4]) / len(q1a[4])) + " - " + str(sum(q2a[4]) / len(q2a[4])) + " - " + str(sum(q3a[4]) / len(q3a[4])))

	print("Limit: " + str(limit) + " First y/n: " + str(first))
	print("Total overhead cost (each address in the path is one byte): " + str(overhead_total))
	print("Average completeness: " + str((sum(routingtablesizes) / 98) / count))

	print("Average time required to learn 75% of the network for a node: " + str(q3time / 98))
	print("Average amount of packets needed to learn 25, 50, 75% of the network for a node respectively: " + str(
		totalq1msgcnt / 98) + " - " + str(totalq2msgcnt / 98) + " - " + str(totalq3msgcnt / 98))
	print("Metadata useful: " + str(overhead_useful))

	t1 = 0
	t2 = 0
	t3 = 0
	t4 = 0
	t5 = 0
	t6 = 0
	t7 = 0
	t8 = 0
	t9 = 0
	for entry in timestampsreceived:
		if entry < 500:
			t1 += 1
		elif entry < 1000:
			t2 += 1
		elif entry < 1500:
			t3 += 1
		elif entry < 2000:
			t4 += 1
		elif entry < 2500:
			t5 += 1
		elif entry < 3000:
			t6 += 1
		elif entry < 3500:
			t7 += 1
		elif entry < 4000:
			t8 += 1
		elif entry < 4500:
			t9 += 1

	labels = ["0 - 500", "500 - 1000", "1000 - 1500", "1500 - 2000", "2000 - 2500", "2500 - 3000", "3000 - 3500",
	          "3500 - 4000", "4000 - 4500"]
	data = [t1, t2, t3, t4, t5, t6, t7, t8, t9]
	plt.xticks(range(len(data)), labels)
	plt.xlabel("Timestamp interval")
	plt.ylabel("Amount of packages received")
	plt.bar(range(len(data)), data)
	plt.show()


if __name__ == '__main__':
	initialize()
